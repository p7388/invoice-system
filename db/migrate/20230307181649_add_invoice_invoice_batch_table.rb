class AddInvoiceInvoiceBatchTable < ActiveRecord::Migration[7.0]
  def change
    create_table :invoice_bags do |t|
      t.integer :invoice_id
      t.integer :invoice_bag_id

      t.timestamps
    end
  end
end
