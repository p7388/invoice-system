class CreateInvoiceBags < ActiveRecord::Migration[7.0]
  def change
    create_table :invoice_bags do |t|
      t.integer :total_amount
      t.string :name

      t.timestamps
    end
  end
end
