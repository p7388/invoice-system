# README

We can run this application using below steps

Steps:

* clone the repository using SSH

* run `bundle install`

* run `rails db:migrate`


Note: `invoice_batches` is named as `invoice_bags` in this application.