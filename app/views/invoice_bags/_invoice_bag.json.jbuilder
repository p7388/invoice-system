json.extract! invoice_bag, :id, :total_amount, :name, :created_at, :updated_at
json.url invoice_bag_url(invoice_bag, format: :json)
