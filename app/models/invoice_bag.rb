class InvoiceBag < ApplicationRecord
  has_many :invoice_invoice_bags, dependent: :destroy
  has_many :invoices, through: :invoice_invoice_bags
end
