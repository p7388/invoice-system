class Invoice < ApplicationRecord

  has_many :invoice_invoice_bags, dependent: :destroy
  has_many :invoice_bags, through: :invoice_invoice_bags

  after_commit: update_invoices

  private

  def update_invoices
    InvoiceReSyncJob.perform_async(self)
  end
end
