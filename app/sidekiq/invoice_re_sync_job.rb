class InvoiceReSyncJob
  include Sidekiq::Job

  def perform(*args)
    invoice = Invoice.find(args[:id])

    invoice.invoice_bags.each do |iv|
      # code to update invoice bags here
    end
  end
end
