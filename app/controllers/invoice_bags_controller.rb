class InvoiceBagsController < ApplicationController
  before_action :set_invoice_bag, only: %i[ show edit update destroy ]

  # GET /invoice_bags or /invoice_bags.json
  def index
    @invoice_bags = InvoiceBag.all
  end

  # GET /invoice_bags/1 or /invoice_bags/1.json
  def show
  end

  # GET /invoice_bags/new
  def new
    @invoice_bag = InvoiceBag.new
  end

  # GET /invoice_bags/1/edit
  def edit
  end

  # POST /invoice_bags or /invoice_bags.json
  def create
    @invoice_bag = InvoiceBag.new(invoice_bag_params)

    respond_to do |format|
      if @invoice_bag.save
        format.html { redirect_to invoice_bag_url(@invoice_bag), notice: "Invoice bag was successfully created." }
        format.json { render :show, status: :created, location: @invoice_bag }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @invoice_bag.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /invoice_bags/1 or /invoice_bags/1.json
  def update
    respond_to do |format|
      if @invoice_bag.update(invoice_bag_params)
        format.html { redirect_to invoice_bag_url(@invoice_bag), notice: "Invoice bag was successfully updated." }
        format.json { render :show, status: :ok, location: @invoice_bag }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @invoice_bag.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /invoice_bags/1 or /invoice_bags/1.json
  def destroy
    @invoice_bag.destroy

    respond_to do |format|
      format.html { redirect_to invoice_bags_url, notice: "Invoice bag was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_invoice_bag
      @invoice_bag = InvoiceBag.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def invoice_bag_params
      params.require(:invoice_bag).permit(:total_amount, :name)
    end
end
