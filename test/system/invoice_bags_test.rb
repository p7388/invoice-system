require "application_system_test_case"

class InvoiceBagsTest < ApplicationSystemTestCase
  setup do
    @invoice_bag = invoice_bags(:one)
  end

  test "visiting the index" do
    visit invoice_bags_url
    assert_selector "h1", text: "Invoice bags"
  end

  test "should create invoice bag" do
    visit invoice_bags_url
    click_on "New invoice bag"

    fill_in "Name", with: @invoice_bag.name
    fill_in "Total amount", with: @invoice_bag.total_amount
    click_on "Create Invoice bag"

    assert_text "Invoice bag was successfully created"
    click_on "Back"
  end

  test "should update Invoice bag" do
    visit invoice_bag_url(@invoice_bag)
    click_on "Edit this invoice bag", match: :first

    fill_in "Name", with: @invoice_bag.name
    fill_in "Total amount", with: @invoice_bag.total_amount
    click_on "Update Invoice bag"

    assert_text "Invoice bag was successfully updated"
    click_on "Back"
  end

  test "should destroy Invoice bag" do
    visit invoice_bag_url(@invoice_bag)
    click_on "Destroy this invoice bag", match: :first

    assert_text "Invoice bag was successfully destroyed"
  end
end
