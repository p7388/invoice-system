require "test_helper"

class InvoiceBagsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @invoice_bag = invoice_bags(:one)
  end

  test "should get index" do
    get invoice_bags_url
    assert_response :success
  end

  test "should get new" do
    get new_invoice_bag_url
    assert_response :success
  end

  test "should create invoice_bag" do
    assert_difference("InvoiceBag.count") do
      post invoice_bags_url, params: { invoice_bag: { name: @invoice_bag.name, total_amount: @invoice_bag.total_amount } }
    end

    assert_redirected_to invoice_bag_url(InvoiceBag.last)
  end

  test "should show invoice_bag" do
    get invoice_bag_url(@invoice_bag)
    assert_response :success
  end

  test "should get edit" do
    get edit_invoice_bag_url(@invoice_bag)
    assert_response :success
  end

  test "should update invoice_bag" do
    patch invoice_bag_url(@invoice_bag), params: { invoice_bag: { name: @invoice_bag.name, total_amount: @invoice_bag.total_amount } }
    assert_redirected_to invoice_bag_url(@invoice_bag)
  end

  test "should destroy invoice_bag" do
    assert_difference("InvoiceBag.count", -1) do
      delete invoice_bag_url(@invoice_bag)
    end

    assert_redirected_to invoice_bags_url
  end
end
